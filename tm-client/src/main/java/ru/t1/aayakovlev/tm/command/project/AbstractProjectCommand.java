package ru.t1.aayakovlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.endpoint.ProjectEndpoint;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.model.Project;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected ProjectEndpoint getProjectEndpoint() {
        return serviceLocator.getProjectEndpoint();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void renderProjects(@NotNull final List<Project> projects) {
        @NotNull final AtomicInteger index = new AtomicInteger(1);
        projects.stream()
                .filter(Objects::nonNull)
                .forEachOrdered((p) -> System.out.println(index.getAndIncrement() + ". " + p));
    }

    protected void showProject(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

}
