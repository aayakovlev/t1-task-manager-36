package ru.t1.aayakovlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextNumber;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Start project by index.";

    @NotNull
    public static final String NAME = "project-start-by-index";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.print("Enter index: ");
        @NotNull final Integer index = nextNumber() - 1;

        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(getToken());
        request.setIndex(index);
        request.setStatus(Status.IN_PROGRESS);

        getProjectEndpoint().changeStatusByIndex(request);
    }

}
