package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.marker.IntegrationCategory;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import java.util.UUID;

import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserEndpointTestConstant.*;

@Category(IntegrationCategory.class)
public final class TaskEndpointImplTest {

    @NotNull
    private final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private final AuthEndpoint authEndpoint = AuthEndpoint.newInstance(propertyService);

    @NotNull
    private final UserEndpoint userEndpoint = UserEndpoint.newInstance(propertyService);

    @NotNull
    private final TaskEndpoint taskEndpoint = TaskEndpoint.newInstance(propertyService);

    @Nullable
    private String adminToken;

    @Nullable
    private String ID;

    @Nullable
    private String DELETE_ID;

    @Before
    public void init() throws AbstractException {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_LOGIN);
        loginRequest.setPassword(ADMIN_PASSWORD);
        adminToken = authEndpoint.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(TEST_LOGIN);
        request.setPassword(TEST_PASSWORD);
        request.setEmail(TEST_EMAIL);
        userEndpoint.registry(request);
        ID = create().getId();
        create();
        DELETE_ID = create().getId();
    }

    @After
    public void after() throws AbstractException {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(TEST_LOGIN);
        userEndpoint.remove(request);
    }

    @NotNull
    private String getToken() throws AbstractException {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(TEST_LOGIN);
        loginRequest.setPassword(TEST_PASSWORD);
        return authEndpoint.login(loginRequest).getToken();
    }

    private Task create() throws AbstractException {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken());
        request.setName("Test Task name");
        request.setDescription("Test Task name");
        return taskEndpoint.create(request).getTask();
    }

    @Test
    public void When_TaskBindToProject_Expect_TaskBindToProjectResponse() throws AbstractException {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken());
        request.setTaskId(ID);
        request.setProjectId(UUID.randomUUID().toString());

        Assert.assertNotNull(taskEndpoint.bindToProject(request));
    }

    @Test
    public void When_TaskChangeStatusById_Expect_TaskChangeStatusByIdResponse() throws AbstractException {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setId(ID);
        request.setStatus(STATUS);

        Assert.assertNotNull(taskEndpoint.changeStatusById(request));
    }

    @Test
    public void When_TaskChangeStatusByIndex_Expect_TaskChangeStatusByIndexResponse() throws AbstractException {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(getToken());
        request.setIndex(INDEX);
        request.setStatus(STATUS);

        Assert.assertNotNull(taskEndpoint.changeStatusByIndex(request));
    }

    @Test
    public void When_TaskCreate_Expect_TaskCreateResponse() throws AbstractException {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken());
        request.setName(NAME);
        request.setDescription(DESCRIPTION);

        Assert.assertNotNull(taskEndpoint.create(request));
    }

    @Test
    public void When_TaskClear_Expect_TaskClearResponse() throws AbstractException {
        @NotNull final TaskClearRequest request = new TaskClearRequest(adminToken);

        Assert.assertNotNull(taskEndpoint.clear(request));
    }

    @Test
    public void When_TaskRemoveById_Expect_TaskRemoveByIdResponse() throws AbstractException {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken());
        request.setId(DELETE_ID);

        Assert.assertNotNull(taskEndpoint.removeById(request));
    }

    @Test
    public void When_TaskRemoveByIndexCommand_Expect_TaskRemoveByIndexResponse() throws AbstractException {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(getToken());
        request.setIndex(INDEX_FOR_REMOVE);

        Assert.assertNotNull(taskEndpoint.removeByIndex(request));
    }

    @Test
    public void When_TaskShowAll_Expect_TaskShowAllResponse() throws AbstractException {
        @NotNull final TaskShowAllRequest request = new TaskShowAllRequest(getToken());
        request.setSort(Sort.BY_NAME);

        Assert.assertNotNull(taskEndpoint.showAll(request));
    }

    @Test
    public void When_TaskShowById_Expect_TaskShowByIdResponse() throws AbstractException {
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken());
        request.setId(ID);

        Assert.assertNotNull(taskEndpoint.showById(request));
    }

    @Test
    public void When_TaskShowByIndex_Expect_TaskShowByIndexResponse() throws AbstractException {
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(getToken());
        request.setIndex(INDEX);

        Assert.assertNotNull(taskEndpoint.showByIndex(request));
    }

    @Test
    public void When_TaskShowByProjectId_Expect_TaskShowByProjectIdResponse() throws AbstractException {
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(getToken());
        request.setProjectId(UUID.randomUUID().toString());
        Assert.assertNotNull(taskEndpoint.showByProjectId(request));
    }

    @Test
    public void When_TaskUnbindFromProject_Expect_TaskUnbindFromProjectResponse() throws AbstractException {
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken());
        request.setTaskId(ID);
        request.setProjectId(UUID.randomUUID().toString());

        Assert.assertNotNull(taskEndpoint.unbindFromProject(request));
    }

    @Test
    public void When_TaskUpdateById_Expect_TaskUpdateByIdResponse() throws AbstractException {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(getToken());
        request.setId(ID);
        request.setName(NAME);
        request.setDescription(DESCRIPTION);

        Assert.assertNotNull(taskEndpoint.updateById(request));
    }

    @Test
    public void When_TaskUpdateByIndex_Expect_TaskUpdateByIndexResponse() throws AbstractException {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(getToken());
        request.setIndex(INDEX);
        request.setName(NAME);
        request.setDescription(DESCRIPTION);

        Assert.assertNotNull(taskEndpoint.updateByIndex(request));
    }

}
