package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.UserRegistryResponse;
import ru.t1.aayakovlev.tm.dto.response.UserRemoveResponse;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.marker.IntegrationCategory;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import static ru.t1.aayakovlev.tm.constant.UserEndpointTestConstant.*;

@Category(IntegrationCategory.class)
public final class UserEndpointImplTest {

    @NotNull
    private final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private final AuthEndpoint authEndpoint = AuthEndpoint.newInstance(propertyService);

    @NotNull
    private final UserEndpoint userEndpoint = UserEndpoint.newInstance(propertyService);

    @Nullable
    private String adminToken;

    @Before
    public void init() throws AbstractException {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_LOGIN);
        loginRequest.setPassword(ADMIN_PASSWORD);
        adminToken = authEndpoint.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(TEST_LOGIN);
        request.setPassword(TEST_PASSWORD);
        request.setEmail(TEST_EMAIL);
        userEndpoint.registry(request);
    }

    @After
    public void after() throws AbstractException {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(TEST_LOGIN);
        userEndpoint.remove(request);
    }

    @NotNull
    private String getToken(@NotNull final String login, @NotNull final String password) throws AbstractException {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(login);
        loginRequest.setPassword(password);
        return authEndpoint.login(loginRequest).getToken();
    }

    @NotNull
    private UserRegistryResponse registry(
    ) throws AbstractException {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(TEST_LOGIN_TWO);
        request.setPassword(TEST_PASSWORD_TWO);
        request.setEmail(TEST_EMAIL_TWO);
        return userEndpoint.registry(request);
    }

    @NotNull
    private UserRemoveResponse remove() throws AbstractException {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken(ADMIN_LOGIN, ADMIN_PASSWORD));
        request.setLogin(TEST_LOGIN_TWO);
        return userEndpoint.remove(request);
    }

    @Test
    public void When_UserChangePassword_Expect_UserChangePasswordResponse() throws AbstractException {
        @NotNull final String userId = registry().getUser().getId();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(
                getToken(TEST_LOGIN_TWO, TEST_PASSWORD_TWO)
        );

        request.setId(userId);
        request.setPassword(TEST_CHANGE_PASSWORD_TWO);

        Assert.assertNotNull(userEndpoint.changePassword(request));
        remove();
    }

    @Test
    public void When_UserLock_Expect_UserLockResponse() throws AbstractException {
        registry();
        @NotNull final UserLockRequest request = new UserLockRequest(getToken(ADMIN_LOGIN, ADMIN_PASSWORD));
        request.setLogin(TEST_LOGIN_TWO);

        Assert.assertNotNull(userEndpoint.lock(request));
        remove();
    }

    @Test
    public void When_UserRegistry_Expect_UserRegistryResponse() throws AbstractException {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken(TEST_LOGIN, TEST_PASSWORD));
        request.setLogin(TEST_LOGIN_TWO);
        request.setPassword(TEST_PASSWORD_TWO);
        request.setEmail(TEST_EMAIL_TWO);

        Assert.assertNotNull(userEndpoint.registry(request));
        remove();
    }

    @Test
    public void When_UserRemove_Expect_UserRemoveResponse() throws AbstractException {
        registry();

        Assert.assertNotNull(remove());
    }

    @Test
    public void When_UserUnlock_Expect_UserUnlockResponse() throws AbstractException {
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(getToken(ADMIN_LOGIN, ADMIN_PASSWORD));
        request.setLogin(TEST_LOGIN);

        Assert.assertNotNull(userEndpoint.unlock(request));
    }

    @Test
    public void When_UserUpdateProfile_Expect_UserUpdateProfileResponse() throws AbstractException {
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(
                getToken(TEST_LOGIN, TEST_PASSWORD)
        );
        request.setFirstName(TEST_FIRST_NAME);
        request.setLastName(TEST_LAST_NAME);
        request.setMiddleName(TEST_MIDDLE_NAME);

        Assert.assertNotNull(userEndpoint.update(request));
    }

}
