package ru.t1.aayakovlev.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.service.TaskService;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.FIRST_ARRAY_ELEMENT_INDEX;

public final class TaskServiceImpl extends AbstractUserOwnedService<Task, TaskRepository> implements TaskService {

    public TaskServiceImpl(@NotNull final TaskRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Task changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final Task model = findById(userId, id);
        model.setStatus(status);
        return model;
    }

    @NotNull
    @Override
    public Task changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final int recordCount = repository.count(userId);
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordCount)
            throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final Task model = findByIndex(userId, index);
        model.setStatus(status);
        return model;
    }

    @Override
    @NotNull
    public Task create(@Nullable final String userId, @Nullable final String name) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    @NotNull
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    @NotNull
    public List<Task> findByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        return repository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task model = findById(userId, id);
        model.setName(name);
        model.setDescription(description);
        return model;
    }

    @NotNull
    @Override
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task model = findByIndex(userId, index);
        model.setName(name);
        model.setDescription(description);
        return model;
    }

}
