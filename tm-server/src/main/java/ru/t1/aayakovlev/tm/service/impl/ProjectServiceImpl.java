package ru.t1.aayakovlev.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.service.ProjectService;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.FIRST_ARRAY_ELEMENT_INDEX;

public final class ProjectServiceImpl extends AbstractUserOwnedService<Project, ProjectRepository> implements ProjectService {

    public ProjectServiceImpl(@NotNull final ProjectRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final Project model = findById(userId, id);
        model.setStatus(status);
        return model;
    }

    @NotNull
    @Override
    public Project changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final int recordCount = repository.count(userId);
        if (index == null || index < FIRST_ARRAY_ELEMENT_INDEX || index >= recordCount)
            throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final Project model = findByIndex(userId, index);
        model.setStatus(status);
        return model;
    }

    @Override
    @NotNull
    public Project create(@Nullable final String userId, @Nullable final String name) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    @NotNull
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project model = findById(userId, id);
        model.setName(name);
        model.setDescription(description);
        return model;
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project model = findByIndex(userId, index);
        model.setName(name);
        model.setDescription(description);
        return model;
    }

}
