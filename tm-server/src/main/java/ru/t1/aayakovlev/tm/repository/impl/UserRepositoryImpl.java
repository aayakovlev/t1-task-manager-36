package ru.t1.aayakovlev.tm.repository.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.UserRepository;

public final class UserRepositoryImpl extends AbstractBaseRepository<User> implements UserRepository {

    @Override
    @Nullable
    public User findByLogin(@NotNull final String login) {
        return models.stream()
                .filter((m) -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull final String email) {
        return models.stream()
                .filter((m) -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean isLoginExists(@NotNull final String login) {
        return models.stream()
                .anyMatch((m) -> login.equals(m.getLogin()));
    }

    @Override
    public boolean isEmailExists(@NotNull final String email) {
        return models.stream()
                .anyMatch((m) -> email.equals(m.getEmail()));
    }

}
