package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.repository.impl.ProjectRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.service.impl.ProjectTaskServiceImpl;

import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_TWO;

@Category(UnitCategory.class)
public final class ProjectTaskServiceImplTest {

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepositoryImpl();

    @NotNull
    private final TaskRepository taskRepository = new TaskRepositoryImpl();

    @NotNull
    private final ProjectTaskService service = new ProjectTaskServiceImpl(projectRepository, taskRepository);

    @Before
    public void init() throws EntityEmptyException {
        projectRepository.save(PROJECT_USER_ONE);
        projectRepository.save(PROJECT_USER_TWO);
        taskRepository.save(TASK_USER_ONE);
        taskRepository.save(TASK_USER_TWO);
    }

    @After
    public void after() {
        projectRepository.removeAll(PROJECT_LIST);
        taskRepository.removeAll(TASK_LIST);
    }

    @Test
    public void When_BindTaskToProject_Expect_TaskWithProjectId() throws AbstractException {
        projectRepository.save(PROJECT_ADMIN_ONE);
        taskRepository.save(TASK_EMPTY_PROJECT_ID);
        @NotNull final Task task = service.bindTaskToProject(
                ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId(), TASK_EMPTY_PROJECT_ID.getId()
        );
        Assert.assertNotNull(task.getProjectId());
    }

    @Test
    public void When_UnbindTaskFromProject_Expect_TaskWithoutProjectId() throws AbstractException {
        projectRepository.save(PROJECT_ADMIN_ONE);
        taskRepository.save(TASK_ADMIN_ONE);
        @NotNull final Task task = service.unbindTaskFromProject(
                ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId(), TASK_ADMIN_ONE.getId()
        );
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void When_RemoveProjectById_Expect_ThrowsEntityNotFoundException() throws EntityEmptyException {
        projectRepository.save(PROJECT_ADMIN_TWO);
        taskRepository.save(TASK_ADMIN_ONE);
        taskRepository.save(TASK_ADMIN_TWO);
        Assert.assertTrue(taskRepository.findAllByProjectId(ADMIN_USER_TWO.getId(), PROJECT_ADMIN_TWO.getId()).isEmpty());
    }

}
