package ru.t1.aayakovlev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.impl.UserRepositoryImpl;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;

@Category(UnitCategory.class)
public final class UserRepositoryImplTest {

    @NotNull
    private final UserRepository repository = new UserRepositoryImpl();

    @Before
    public void init() throws EntityEmptyException {
        repository.save(COMMON_USER_ONE);
        repository.save(COMMON_USER_TWO);
    }

    @After
    public void after() {
        repository.removeAll(USER_LIST);
    }

    @Test
    public void When_FindByIdExistsUser_Expect_ReturnUser() throws AbstractException {
        @Nullable final User user = repository.findById(COMMON_USER_ONE.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(COMMON_USER_ONE.getEmail(), user.getEmail());
        Assert.assertEquals(COMMON_USER_ONE.getFirstName(), user.getFirstName());
        Assert.assertEquals(COMMON_USER_ONE.getLastName(), user.getLastName());
        Assert.assertEquals(COMMON_USER_ONE.getMiddleName(), user.getMiddleName());
        Assert.assertEquals(COMMON_USER_ONE.getLogin(), user.getLogin());
    }

    @Test
    public void When_FindByIdExistsUser_Expect_ReturnNull() throws AbstractException {
        @Nullable final User user = repository.findById(USER_ID_NOT_EXISTED);
        Assert.assertNull(user);
    }

    @Test
    public void When_SaveNotNullUser_Expect_ReturnUser() throws AbstractException {
        @NotNull final User savedUser = repository.save(ADMIN_USER_ONE);
        Assert.assertNotNull(savedUser);
        Assert.assertEquals(ADMIN_USER_ONE, savedUser);
        @Nullable final User user = repository.findById(ADMIN_USER_ONE.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_USER_ONE, user);
    }

    @Test
    public void When_ExistsByIdExistedUser_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdNotExistedUser_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(USER_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdExistedUser_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdNotExistedUser_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(USER_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListUsers() {
        final List<User> users = repository.findAll();
        Assert.assertArrayEquals(users.toArray(), COMMON_USER_LIST.toArray());
    }

    @Test
    public void When_RemoveExistedUser_Expect_ReturnUser() throws AbstractException {
        Assert.assertNotNull(repository.save(ADMIN_USER_ONE));
        Assert.assertNotNull(repository.remove(ADMIN_USER_ONE));
    }

    @Test
    public void When_RemoveAll_Expect_NullUsers() throws AbstractException {
        repository.save(ADMIN_USER_ONE);
        repository.save(ADMIN_USER_TWO);
        repository.removeAll(ADMIN_USER_LIST);
        Assert.assertNull(repository.findById(ADMIN_USER_ONE.getId()));
        Assert.assertNull(repository.findById(ADMIN_USER_TWO.getId()));
    }

    @Test
    public void When_RemoveByIdExistedUser_Expect_User() throws AbstractException {
        Assert.assertNotNull(repository.save(ADMIN_USER_ONE));
        Assert.assertNotNull(repository.removeById(ADMIN_USER_ONE.getId()));
    }

    @Test
    public void When_RemoveByIdNotExistedUser_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.removeById(USER_ID_NOT_EXISTED)
        );
    }

}
