package ru.t1.aayakovlev.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskCompleteByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private Status status;

    public TaskCompleteByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
