package ru.t1.aayakovlev.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectCompleteByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectCompleteByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
